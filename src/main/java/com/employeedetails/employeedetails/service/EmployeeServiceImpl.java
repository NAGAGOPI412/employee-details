package com.employeedetails.employeedetails.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employeedetails.employeedetails.bean.Employee;
import com.employeedetails.employeedetails.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	EmployeeRepository repo;

	@Override
	public Employee create(Employee employee) {
		return repo.save(employee);
	}

	@Override
	public Iterable<Employee> findAll() {
		return repo.findAll();
	}

}
