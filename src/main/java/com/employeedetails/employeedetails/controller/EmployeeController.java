package com.employeedetails.employeedetails.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.employeedetails.employeedetails.bean.Employee;
import com.employeedetails.employeedetails.service.EmployeeService;

@Controller
@CrossOrigin
public class EmployeeController {
	@Autowired
	EmployeeService service;

	@PostMapping("/employee/create")
	public ResponseEntity<Employee> create(@RequestBody Employee employee) {
		return new ResponseEntity<Employee>(service.create(employee), HttpStatus.OK);
	}

	@GetMapping("/employee/findAll")
	public ResponseEntity<Iterable<Employee>> findAll() {
		return new ResponseEntity<Iterable<Employee>>(service.findAll(), HttpStatus.OK);
	}

	
}
